import { combineReducers, createStore } from "redux";
import TaskEvent from "../components/TaskEvent"

const appReducer = combineReducers({
    TaskReducer: TaskEvent 
})
const store = createStore(
    appReducer,
)

export default store