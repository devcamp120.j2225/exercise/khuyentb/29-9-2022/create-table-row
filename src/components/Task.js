import React from "react"
import { useDispatch, useSelector } from "react-redux"
import { Container, Grid, Input, Button, MenuList, MenuItem, FormLabel, TableHead, TableCell, TableContainer, Table, TableRow, TableBody } from '@mui/material';

const Task = () => {

    const dispatch = useDispatch();
    const { tasks, taskName } = useSelector((reduxData) => reduxData.TaskReducer);

    const taskNameHandler = (event) => {
        dispatch({
            type: "VALUE_HANDLER",
            payload: {
                taskName: event.target.value
            }
        })
    }

    const addTaskHandler = (event) => {
        event.preventDefault();
        let id = 0

        if (tasks.length) {
            id = Math.max(...tasks.map(task => task.id));
            id++
        }

        if (taskName) {
            dispatch({
                type: "ADD_TASK",
                payload: {
                    task: {
                        name: taskName,
                        completed: false,
                        id
                    }
                }
            })
            dispatch({
                type: "VALUE_HANDLER",
                payload: {
                    taskName: ""
                }
            })
        }
    }

    const taskCompletedHandler = (event) => {
        dispatch({
            type: "TOGGLE_COMPLETETED_TASK",
            payload: {
                id: event.target.id
            }
        })
    }



    return (
        <Container>
            <Grid container spacing={2} style={{ margin: '0 auto', padding: 20 }}>
                <Grid item xs={12}>
                    <form onSubmit={addTaskHandler}>
                        <Grid container xs={12}>
                            <Grid item xs={4}>
                                <FormLabel>Nhập Nội Dung Dòng Mới</FormLabel>
                            </Grid>
                            <Grid item xs={4}>
                                <Input value={taskName} onChange={taskNameHandler} style={{ width: '90%' }} />
                            </Grid>
                            <Grid item xs={4}>
                                <Button variant="contained" type="submit">Thêm</Button>
                            </Grid>
                        </Grid>
                    </form>
                </Grid>
            </Grid>
            <TableContainer>
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell>
                                STT
                            </TableCell>
                            <TableCell>
                                Noi Dung
                            </TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {tasks.map((value, index) => {
                            return (
                                <TableRow key={index}>
                                    <TableCell>
                                        {index + 1}
                                    </TableCell>
                                    <TableCell style={{color: value.completed ? "green" : "red"}} onClick={taskCompletedHandler} id={value.id}>
                                        {value.name}
                                    </TableCell>
                                </TableRow>
                            )
                        })}
                    </TableBody>
                </Table>
            </TableContainer>
        </Container>
    )
}

export default Task